
	<footer>
		<section class="ftr-area ftr--blind" id="footer">
			<div class="container">
				<div class="ftr__box col-sm-4 ftr__logo">
					<h4>About Company</h4>
					<div class="ftr-logo gray-img">
						<a href="#">
							<img src="images/logo.png" alt="ftr logo"/>
						</a>
					</div>
					<p>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since. When an unknown printer took a galley of type and scrambled it to make a type specimen book.
					</p>
					<div class="ftr-social-icons">
						<ul>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram"></i></a></li>								
						</ul>
					</div>
				</div>
				<div class="ftr__box col-sm-3 clrlist listview">
					<h4>About Us</h4>
					<ul class="ftr__box__list">
						<li><a href="#">About Us</a></li>
						<li><a href="#">Contest</a></li>
						<li><a href="#">Deals</a></li>
						<li><a href="#">Blogs</a></li>
					</ul>
				</div>
				<div class="ftr__box col-sm-2 clrlist listview dotlist--connect">
					<h4>Help</h4>
					<ul class="ftr__box__list">
						<li><a href="#">Create Events</a></li>
						<li><a href="#">Pricing</a></li>
						<li><a href="#">Contact Us</a></li>
						<li><a href="#">Advertise</a></li>
						<li><a href="#">FAQ</a></li>
					</ul>
				</div>
				<div class="ftr__box shake-icon col-sm-3 social-area clrlist ">
					<h4>Related Links</h4>
					<ul class="ftr__box__list">
						<li><a href="#">Terms & Conditons</a></li>
						<li><a href="#">Privacy & Cookies</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>	
				<div class="clearfix"></div>
						
			</div>
			<div class="clearfix"></div>
			<div class="btm-cont col-sm-12">
				<p>&copy; 2016 Ottawa Perks All rights reserved.</p>
			</div>	

		</section>
				
	</footer>
	
	
			<a href="" class="scrollToTop"><i class="fa fa-arrow-up"></i></a>

			

			

</main>
    
	<!--Bootstrap-->
    <script src="js/bootstrap.min.js"></script>
	<!--./Bootstrap-->
	
	<!--Major Scripts-->
	<script src="js/viewportchecker.js"></script>
    <script src="js/kodeized.js"></script>
	<!--./Major Scripts-->


	
	
		</body>
</html>