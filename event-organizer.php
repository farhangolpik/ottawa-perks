<?php include("header.php"); ?>
	
	<section class="main-area admin-area mb40">
		<div class="container">
			
			<div class="bg-white col-sm-12">
			
			<?php include("admin-nav.php"); ?>
			
			<div class="admin__rgt col-sm-9 bg-white pr0">
				
				<div class="organizer__top mb20 mt20 overload">
				  <button type="button" class="btn btn-success pul-rgt"> Create an Event </button>
				</div>
				
				<div class="organizer__lists mb30">
				
					<table class="table table-bordered table-valign table-striped">
						<tr>
							<td><span class="pic"><img class="media-object" src="images/moreAd4.jpg" alt="..."></span></td>
							<td>Event One</td>
							<td><button type="button" class="btn btn-primary"> <i class="fa fa-hdd-o"></i> Unpublish </button></td>
							<td><button type="button" class="btn btn-success"> <i class="fa fa-plus"></i> Add Addons</button></td>
							<td><button type="button" class="btn"> <i class="fa fa-pencil"></i> Edit </button></td>
						</tr>
						<tr>
							<td><span class="pic"><img class="media-object" src="images/moreAd4.jpg" alt="..."></span></td>
							<td>Event One</td>
							<td><button type="button" class="btn btn-primary"> <i class="fa fa-hdd-o"></i> Unpublish </button></td>
							<td><button type="button" class="btn btn-success"> <i class="fa fa-plus"></i> Add Addons</button></td>
							<td><button type="button" class="btn"> <i class="fa fa-pencil"></i> Edit </button></td>
						</tr>
						<tr>
							<td><span class="pic"><img class="media-object" src="images/moreAd4.jpg" alt="..."></span></td>
							<td>Event One</td>
							<td><button type="button" class="btn btn-primary"> <i class="fa fa-hdd-o"></i> Unpublish </button></td>
							<td><button type="button" class="btn btn-success"> <i class="fa fa-plus"></i> Add Addons</button></td>
							<td><button type="button" class="btn"> <i class="fa fa-pencil"></i> Edit </button></td>
						</tr>
						
						<tr>
							<td><span class="pic"><img class="media-object" src="images/moreAd4.jpg" alt="..."></span></td>
							<td>Event One</td>
							<td><button type="button" class="btn btn-primary"> <i class="fa fa-hdd-o"></i> Unpublish </button></td>
							<td><button type="button" class="btn btn-success"> <i class="fa fa-plus"></i> Add Addons</button></td>
							<td><button type="button" class="btn"> <i class="fa fa-pencil"></i> Edit </button></td>
						</tr>
						
					</table>
				
				</div>

				
			</div>
			
			</div>
			
			<div class="addons-area bg-white col-sm-12 mt50 p30">
					
					<div class="row">
					
					<div class="addon-box col-sm-3 checker-area">
						<div class="addon__inr">
							<h3>Addon Title</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipi cinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
							<div class="select-area"><input type="checkbox" class="addon__select" id="addon1"><label for="addon1">Buy</label></div>
						</div>
					</div>
					
					<div class="addon-box col-sm-3 checker-area">
						<div class="addon__inr">
							<h3>Addon Title</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipi cinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
							<div class="select-area"><input type="checkbox" class="addon__select" id="addon2"><label for="addon2">Buy</label></div>
						</div>
					</div>
					
					<div class="addon-box col-sm-3 checker-area">
						<div class="addon__inr">
							<h3>Addon Title</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipi cinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
							<div class="select-area"><input type="checkbox" class="addon__select" id="addon3"><label for="addon3">Buy</label></div>
						</div>
					</div>
					
					<div class="addon-box col-sm-3 checker-area" >
						<div class="addon__inr">
							<h3>Addon Title</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipi cinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
							<div class="select-area"><input type="checkbox" class="addon__select" id="addon4"><label for="addon4">Buy</label></div>
						</div>
					</div>
					
					</div>
				
				</div>
				
			
		</div>
	</section>
	
	
	
	
	
<?php include("footer.php"); ?>