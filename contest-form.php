<?php include("header.php"); ?>
	
	<section class="main-area admin-area mb40">
		<div class="container">
			
			<div class="bg-white col-sm-12">
			
			<?php include("admin-nav.php"); ?>
			
			<div class="admin__rgt col-sm-9 bg-white pr0">
				
				<h2>Contest Form</h2>
				
				<div class="form-group col-sm-4 p0">
					<h5>Select Your Event</h5>
					<select class="form-control">
						<option>Select Your Event 1<option>
						<option>Select Your Event 2<option>
						<option>Select Your Event 3<option>
						<option>Select Your Event 4<option>
					</select>
				</div>
				
				<div class="clearfix"></div>
				
				
				<div class="fom-area">
					<form>
						<div class="form-group">
							<label>Contest title</label>
							<input type="text" class="form-control" placeholder="Contest title " />
						</div>
						<div class="form-group">
						<label>Contest slug</label>
							<input type="text" class="form-control" placeholder="Contest slug" />
						</div>
						<div class="form-group">
							<label>Date range</label>
							<input type="text" name="daterange" class="form-control" placeholder="Date range" value="01/01/2015 1:30 PM - 01/01/2015 2:00 PM" />
						</div>
						
						<div class="form-group">
							<label>Contest description</label>
							<textarea class="form-control" placeholder="Contest description" ></textarea>							
						</div>
						
						<div class="form-group">
							<label>Contest reward description</label>
							<textarea class="form-control" placeholder="Contest reward description" ></textarea>							
						</div>
												
						<div class="form-group">
							<label>Twitter hashtag </label>
							<input type="text" class="form-control" placeholder="Twitter hashtag " />
						</div>
						
						
						<div class="form-group">
							<label>Contest add-ons</label>
							<input type="text" class="form-control" placeholder="Contest add-ons" />
						</div>
						
						
						<div class="form-group">
							<button class="btn btn-lg btn-primary">Submit</button>
						</div>
					</form>
				</div>
				
			</div>
			
			</div>
			
			
		</div>
	</section>
	
			<script type="text/javascript">
			jQuery(function() {
				jQuery('input[name="daterange"]').daterangepicker({
					timePicker: true,
					timePickerIncrement: 30,
					locale: {
						format: 'MM/DD/YYYY h:mm A'
					}
				});
			});
			</script>
	
	
	
<?php include("footer.php"); ?>