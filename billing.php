<?php include("header.php"); ?>
	
	<section class="main-area admin-area mb40">
		<div class="container">
			
			<div class="bg-white col-sm-12">
			
			<?php include("admin-nav.php"); ?>
			
			<div class="admin__rgt col-sm-9 bg-white pr0">
				
				<div class="billing__top col-sm-6 p0 mt20  mb20">
					<div class="billing__pic col-sm-3 p0 pic100x100" >
						<img src="images/moreAd4.jpg" alt="" />
					</div>
					<div class="billing__pic col-sm-9  ">
						<h3>Event Title</h3>
						<p>Event Description</p>
					</div>
				</div>
				
				<div class="clearfix"></div>
				
				<div class="addon__lists mb30 table-last-td-right">
					<h3>Apply Addons Detail</h3>
					
					<table class="table table-valign bdr1 clrhm">
						<tr>
							<td class="strong"><h4>Addon Name</h4> Validity and description</td>
							<td class="strong">$19</td>
						</tr>
						<tr>
							<td>Coupon Code</td>
							<td>-$3</td>
						</tr>
						<tr>
							<td class="strong">TOTAL</td>
							<td class="strong">$16</td>
						</tr>
					</table>
					
					<table class="table table-valign bdr1 clrhm">
						<tr>
							<td class="strong"><h4>Addon Name</h4> Validity and description</td>
							<td class="strong">$19</td>
						</tr>
						<tr>
							<td>Coupon Code</td>
							<td>-$3</td>
						</tr>
						<tr>
							<td class="strong">TOTAL</td>
							<td class="strong">$16</td>
						</tr>
					</table>
					
					<table class="table table-valign bdr1 clrhm">
						<tr>
							<td class="h3">Grand Total</td>
							<td class="h3 text-right">$32</td>
						</tr>
					</table>
					
					<div class="form-group">
						<input type="checkbox" id="check1"><label for="check1">Recurring billing checkbox</label>
					</div>
				
				
					<div class="btns text-center">
						<button class="btn btn-lg btn-primary">Not Now</button>
						<button class="btn btn-lg btn-success">Make Payment</button>
					</div>
				</div>

				
			</div>
			
			</div>
			
			
		</div>
	</section>
	
	
	
	
	
<?php include("footer.php"); ?>