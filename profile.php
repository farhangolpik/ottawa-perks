<?php include("header.php"); ?>
	
	<section class="main-area admin-area mb40">
		<div class="container">
			
			<div class="bg-white col-sm-12">
			
			<?php include("admin-nav.php"); ?>
			
			<div class="admin__rgt col-sm-9 bg-white pr0">
				
				<div class="organizer__top mb20 mt20 inline-h">
					<h2>Profile Eidt</h2>
				</div>
				
				<div class="organizer__lists mb30">
				
					<div class="media profile__pic ">
					  <a class="pull-left pic200x200" href="#">
						<img class="media-object" src="images/comingEvent3.jpg" alt="...">
					  </a>
					  <div class="media-body">
						<h4 class="media-heading">User heading</h4>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also  ker including versions of Lorem Ipsum</p>
					  </div>
					</div>
					
					<div class="changepass-area col-sm-6 p0">
						<h4>Change Password</h4>
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control" >
							</div>
							
							<div class="form-group">
								<label>Confirm Password</label>
								<input type="password" class="form-control" >
							</div>
					</div>
					
					<div class="form-group">
						<button class="btn btn-primary btn-lg">Save</button>
					</div>
					
				</div>

				
			</div>
			
			</div>
			
			
		</div>
	</section>
	
	
	
	
	
<?php include("footer.php"); ?>