<!DOCTYPE html>
<html lang="en" class="livewindowsize>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--iPhone from zooming form issue (maximum-scale=1, user-scalable=0)-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <title>Home</title>

	<link rel="icon" type="image/png" href="images/favicon.png">
	
    <!-- Bootstrap --><link href="css/bootstrap.min.css" rel="stylesheet">
	
	<link rel="stylesheet" href="css/colorized.css">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/slidenav.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/daterangepicker.css">
		<!--link rel="stylesheet" href="css/foundation-icons.css"-->
		<!--link rel="stylesheet" type="text/css" href="css/flaticon.css"--> 
	
	<!-- jQuery -->
	<script src="js/jquery-2.2.4.min.js"></script>
	
	<!--browser selector-->
	<script src="js/css_browser_selector.js" type="text/javascript"></script>
	
	<script src="js/moment.min.js" type="text/javascript"></script>
	
	<script src="js/daterangepicker.js" type="text/javascript"></script>
	
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	
  </head>
  <body class="transition nav-plusminus slide-navbar slide-navbar--right">

<header>
	<section class="alert">
		<div class="container">
			<div class="signup btn-group pul-rgt">
				<ul>
					<li><a href="user2.php"><i class="fa fa-sign-in"></i>Login</a></li>
					<li><a href="#"><i class="fa fa-user"></i>Register</a></li>
				</ul>
			</div>
		</div>
	</section>
	
	<section class="hdr-area hdr-nav hdr--sticky blur-nav--hover0 cross-toggle bg-white" data-navitemlimit="7">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation" id="slide-nav">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
					<a class="navbar-brand" href="./"><img src="images/logo.png" alt="logo" /></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div id="slidemenu">
					<div class="collapse navbar-collapse listdvr" id="bs-example-navbar-collapse-1">
					  <ul class="nav navbar-nav navbar-main navbar-move">
						<li><a href="index.html">Event page</a></li>
						<li class=""><a href="#!2">Business Listings</a></li>
						<li><a href="#!3">Business Promotions</a></li>
						<li><a href="#!5">Activity Programs</a></li>
						<li><a href="#!6">Kids Corner</a></li>
						<li><a href="#!7">Contest</a></li>
					  </ul>
					  
						
						
					<form action="#" class="search-fom expand-search expand-search--fullwidth pul-rgt col-sm-2">
						<div class="form-group create-event has-feedback">
							<a href="#">Create Event</a>
						</div>
					</form>


					</div><!-- /.navbar-collapse -->
				</div>
			  </div><!-- /.container-fluid -->
			</nav>
		</div>
	</section>
</header>

<main id="page-content">
	
	