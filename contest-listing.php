<?php include("header.php"); ?>
	
	<section class="main-area admin-area mb40">
		<div class="container">
			
			<div class="bg-white col-sm-12">
			
			<?php include("admin-nav.php"); ?>
			
			<div class="admin__rgt col-sm-9 bg-white pr0">
				
				<div class="organizer__top mb20 mt20 overload inline-h">
					<h2>Contest Form</h2>
					<button type="button" class="btn btn-success pul-rgt"> Create an Event </button>
				</div>
				
				<div class="organizer__lists mb30">
				
					<table class="table table-bordered table-valign table-striped">
						<tr>
							<td><span class="pic"><img class="media-object" src="images/moreAd4.jpg" alt="..."></span></td>
							<td> Contest Title</td>
							<td><button type="button" class="btn btn-primary"> <i class="fa fa-hdd-o"></i> Unpublish </button></td>
							<td><button type="button" class="btn"> <i class="fa fa-pencil"></i> Edit </button></td>
						</tr>
						<tr>
							<td><span class="pic"><img class="media-object" src="images/moreAd4.jpg" alt="..."></span></td>
							<td> Contest Title</td>
							<td><button type="button" class="btn btn-primary"> <i class="fa fa-hdd-o"></i> Unpublish </button></td>
							<td><button type="button" class="btn"> <i class="fa fa-pencil"></i> Edit </button></td>
						</tr>
						<tr>
							<td><span class="pic"><img class="media-object" src="images/moreAd4.jpg" alt="..."></span></td>
							<td> Contest Title</td>
							<td><button type="button" class="btn btn-primary"> <i class="fa fa-hdd-o"></i> Unpublish </button></td>
							<td><button type="button" class="btn"> <i class="fa fa-pencil"></i> Edit </button></td>
						</tr>
						
						<tr>
							<td><span class="pic"><img class="media-object" src="images/moreAd4.jpg" alt="..."></span></td>
							<td> Contest Title</td>
							<td><button type="button" class="btn btn-primary"> <i class="fa fa-hdd-o"></i> Unpublish </button></td>
							<td><button type="button" class="btn"> <i class="fa fa-pencil"></i> Edit </button></td>
						</tr>
						
					</table>
				
				</div>

				
			</div>
			
			</div>
			
			
		</div>
	</section>
	
	
	
	
	
<?php include("footer.php"); ?>