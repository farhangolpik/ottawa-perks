<?php include("header.php"); ?>
	
	<section class="main-area admin-area mb40">
		<div class="container">
			
			<div class="bg-white col-sm-12">
			
			<?php include("admin-nav.php"); ?>
			
			<div class="admin__rgt col-sm-9 bg-white pr0">
				
				
				
			<div class="addons-area bg-white col-sm-12 p0 mb30">
					<h2>Addon</h2>
					
					<div class="form-group">
						<label>Select Your Event</select>
						<select class="form-control">
							<option>Select Your Event Option 1</option>
							<option>Select Your Event Option 2</option>
							<option>Select Your Event Option 3</option>
						</select>
					</div>
					
					<h3>Select Addon</h3>
					<div class="row">
					
					<div class="addon-box col-sm-3 checker-area">
						<div class="addon__inr">
							<h3>Addon Title</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipi cinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
							<div class="select-area"><input type="checkbox" class="addon__select" id="addon1"><label for="addon1">Buy</label></div>
						</div>
					</div>
					
					<div class="addon-box col-sm-3 checker-area">
						<div class="addon__inr">
							<h3>Addon Title</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipi cinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
							<div class="select-area"><input type="checkbox" class="addon__select" id="addon2"><label for="addon2">Buy</label></div>
						</div>
					</div>
					
					<div class="addon-box col-sm-3 checker-area">
						<div class="addon__inr">
							<h3>Addon Title</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipi cinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
							<div class="select-area"><input type="checkbox" class="addon__select" id="addon3"><label for="addon3">Buy</label></div>
						</div>
					</div>
					
					<div class="addon-box col-sm-3 checker-area" >
						<div class="addon__inr">
							<h3>Addon Title</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipi cinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
							<div class="select-area"><input type="checkbox" class="addon__select" id="addon4"><label for="addon4">Buy</label></div>
						</div>
					</div>
					
					</div>
				
				
						<div class="organizer__top mt20   overload">
						  <button type="button" class="btn btn-success pul-rgt"> Apply </button>
						</div>
						
				
				</div>
				

				
			</div>
			
			</div>
			
			
		</div>
	</section>
	
	
	
	
	
<?php include("footer.php"); ?>