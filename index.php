<?php include("header.php"); ?>
	
	<section class="main-area">
		<div class="container">
			
			<div class="left-side col-sm-8">
				<section class="bnr-area">
					<div class="drp-shad">
						<img src="images/banner.jpg" alt="banner">
					</div>					
				</section>
	
				<section class="feat-area slider clrlist">
					<div class="container0">
						<div class="feat__title">
							<h1>Featured Events</h1>
						</div>
						<div id="carousel-example-generic5" class="carousel slide" data-ride="carousel">
		  

		  <!-- Wrapper for slides -->
						  <div class="carousel-inner">


							<div class="item active client-bg">
								<div class="row">
										
									<div class="feat-box col-sm-4">
										<div class="feat__inr rotate-img--hover">
										
											<div class="feat__img">
												<img src="images/featEvent1.jpg" alt="feature-event">
											</div>
											
											<div class="feat__share">
												<div class="feat__date col-sm-3">
													<strong>26</strong> Oct
												</div>
												<div class="feat__icons clrlist col-sm-9">
													<ul>
														<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
														<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
														<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
														<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
											
											<div class="feat__cont">
												<h3>Burlesque Wednesdays</h3>
												<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
												<div class="lnk-btn more-btn">
													<a href="#">Read More</a>
												</div>
											</div>
										</div>
									</div>
									
									<div class="feat-box col-sm-4">
										<div class="feat__inr rotate-img--hover">
										
											<div class="feat__img">
												<img src="images/featEvent2.jpg" alt="feature-event">
											</div>
											
											<div class="feat__share">
												<div class="feat__date col-sm-3">
													<strong>26</strong> Oct
												</div>
												<div class="feat__icons clrlist col-sm-9">
													<ul>
														<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
														<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
														<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
														<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
											
											<div class="feat__cont">
												<h3>Burlesque Wednesdays</h3>
												<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
												<div class="lnk-btn more-btn">
													<a href="#">Read More</a>
												</div>
											</div>
										</div>
									</div>
									
									<div class="feat-box col-sm-4">
										<div class="feat__inr rotate-img--hover">
										
											<div class="feat__img">
												<img src="images/featEvent3.jpg" alt="feature-event">
											</div>
											
											<div class="feat__share">
												<div class="feat__date col-sm-3">
													<strong>26</strong> Oct
												</div>
												<div class="feat__icons clrlist col-sm-9">
													<ul>
														<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
														<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
														<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
														<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
											
											<div class="feat__cont">
												<h3>Burlesque Wednesdays</h3>
												<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
												<div class="lnk-btn more-btn">
													<a href="#">Read More</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="item  client-bg">
								<div class="row">
										
									<div class="feat-box col-sm-4">
										<div class="feat__inr rotate-img--hover">
										
											<div class="feat__img">
												<img src="images/featEvent1.jpg" alt="feature-event">
											</div>
											
											<div class="feat__share">
												<div class="feat__date col-sm-3">
													<strong>26</strong> Oct
												</div>
												<div class="feat__icons clrlist col-sm-9">
													<ul>
														<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
														<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
														<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
														<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
											
											<div class="feat__cont">
												<h3>Burlesque Wednesdays</h3>
												<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
												<div class="lnk-btn more-btn">
													<a href="#">Read More</a>
												</div>
											</div>
										</div>
									</div>
									
									<div class="feat-box col-sm-4">
										<div class="feat__inr rotate-img--hover">
										
											<div class="feat__img">
												<img src="images/featEvent2.jpg" alt="feature-event">
											</div>
											
											<div class="feat__share">
												<div class="feat__date col-sm-3">
													<strong>26</strong> Oct
												</div>
												<div class="feat__icons clrlist col-sm-9">
													<ul>
														<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
														<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
														<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
														<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
											
											<div class="feat__cont">
												<h3>Burlesque Wednesdays</h3>
												<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
												<div class="lnk-btn more-btn">
													<a href="#">Read More</a>
												</div>
											</div>
										</div>
									</div>
									
									<div class="feat-box col-sm-4">
										<div class="feat__inr rotate-img--hover">
										
											<div class="feat__img">
												<img src="images/featEvent3.jpg" alt="feature-event">
											</div>
											
											<div class="feat__share">
												<div class="feat__date col-sm-3">
													<strong>26</strong> Oct
												</div>
												<div class="feat__icons clrlist col-sm-9">
													<ul>
														<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
														<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
														<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
														<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
											
											<div class="feat__cont">
												<h3>Burlesque Wednesdays</h3>
												<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
												<div class="lnk-btn more-btn">
													<a href="#">Read More</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>	

							<div class="item  client-bg">
								<div class="row">
										
											<div class="feat-box col-sm-4">
												<div class="feat__inr rotate-img--hover">
												
													<div class="feat__img">
														<img src="images/featEvent1.jpg" alt="feature-event">
													</div>
													
													<div class="feat__share">
														<div class="feat__date col-sm-3">
															<strong>26</strong> Oct
														</div>
														<div class="feat__icons clrlist col-sm-9">
															<ul>
																<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
																<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
																<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
																<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
															</ul>
														</div>
														<div class="clearfix"></div>
													</div>
													
													<div class="feat__cont">
														<h3>Burlesque Wednesdays</h3>
														<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
														<div class="lnk-btn more-btn">
															<a href="#">Read More</a>
														</div>
													</div>
												</div>
											</div>
											
											<div class="feat-box col-sm-4">
												<div class="feat__inr rotate-img--hover">
												
													<div class="feat__img">
														<img src="images/featEvent2.jpg" alt="feature-event">
													</div>
													
													<div class="feat__share">
														<div class="feat__date col-sm-3">
															<strong>26</strong> Oct
														</div>
														<div class="feat__icons clrlist col-sm-9">
															<ul>
																<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
																<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
																<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
																<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
															</ul>
														</div>
														<div class="clearfix"></div>
													</div>
													
													<div class="feat__cont">
														<h3>Burlesque Wednesdays</h3>
														<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
														<div class="lnk-btn more-btn">
															<a href="#">Read More</a>
														</div>
													</div>
												</div>
											</div>
											
											<div class="feat-box col-sm-4">
												<div class="feat__inr rotate-img--hover">
												
													<div class="feat__img">
														<img src="images/featEvent3.jpg" alt="feature-event">
													</div>
													
													<div class="feat__share">
														<div class="feat__date col-sm-3">
															<strong>26</strong> Oct
														</div>
														<div class="feat__icons clrlist col-sm-9">
															<ul>
																<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
																<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
																<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
																<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
															</ul>
														</div>
														<div class="clearfix"></div>
													</div>
													
													<div class="feat__cont">
														<h3>Burlesque Wednesdays</h3>
														<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
														<div class="lnk-btn more-btn">
															<a href="#">Read More</a>
														</div>
													</div>
												</div>
											</div>
										</div>
							</div>

							<div class="item  client-bg">
								<div class="row">
										
											<div class="feat-box col-sm-4">
												<div class="feat__inr rotate-img--hover">
												
													<div class="feat__img">
														<img src="images/featEvent1.jpg" alt="feature-event">
													</div>
													
													<div class="feat__share">
														<div class="feat__date col-sm-3">
															<strong>26</strong> Oct
														</div>
														<div class="feat__icons clrlist col-sm-9">
															<ul>
																<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
																<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
																<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
																<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
															</ul>
														</div>
														<div class="clearfix"></div>
													</div>
													
													<div class="feat__cont">
														<h3>Burlesque Wednesdays</h3>
														<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
														<div class="lnk-btn more-btn">
															<a href="#">Read More</a>
														</div>
													</div>
												</div>
											</div>
											
											<div class="feat-box col-sm-4">
												<div class="feat__inr rotate-img--hover">
												
													<div class="feat__img">
														<img src="images/featEvent2.jpg" alt="feature-event">
													</div>
													
													<div class="feat__share">
														<div class="feat__date col-sm-3">
															<strong>26</strong> Oct
														</div>
														<div class="feat__icons clrlist col-sm-9">
															<ul>
																<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
																<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
																<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
																<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
															</ul>
														</div>
														<div class="clearfix"></div>
													</div>
													
													<div class="feat__cont">
														<h3>Burlesque Wednesdays</h3>
														<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
														<div class="lnk-btn more-btn">
															<a href="#">Read More</a>
														</div>
													</div>
												</div>
											</div>
											
											<div class="feat-box col-sm-4">
												<div class="feat__inr rotate-img--hover">
												
													<div class="feat__img">
														<img src="images/featEvent3.jpg" alt="feature-event">
													</div>
													
													<div class="feat__share">
														<div class="feat__date col-sm-3">
															<strong>26</strong> Oct
														</div>
														<div class="feat__icons clrlist col-sm-9">
															<ul>
																<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
																<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
																<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
																<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
															</ul>
														</div>
														<div class="clearfix"></div>
													</div>
													
													<div class="feat__cont">
														<h3>Burlesque Wednesdays</h3>
														<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
														<div class="lnk-btn more-btn">
															<a href="#">Read More</a>
														</div>
													</div>
												</div>
											</div>
										</div>
							</div>			
							
						  </div>

						  <a class="left carousel-control" href="#carousel-example-generic5" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left fa fa-angle-left"></span>
						  </a>
						  <a class="right carousel-control" href="#carousel-example-generic5" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right fa fa-angle-right"></span>
						  </a>
		</div>
					</div>
				</section>
				
				<section class="party-area">
					<div class="party__bnr">
						<img src="images/partyBanner.jpg" alt="party">
					</div>
				</section>
				
				<section class="search-area clrlist">
					<div class="srch__words col-sm-4">
						<div class="input-group">
							<input type="text" class="form-control" aria-label="..." placeholder="Key Words.....">
						</div>
					</div>
					<div class="select__ctg col-sm-4">
						<div class="input-group">
							<select class="form-control" id="sel1">
								<option>SELECT CATEGORY</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>
						</div>
					</div>
					<div class="srch__btn__area col-sm-4">
						<div class="srch__date__icon col-sm-4">
							<img src="images/calendarIcon.png" alt="calendar-clock-icon">
						</div>
						<div class="srch__btn col-sm-8">
							<button type="button">Search</button>
						</div>
					</div>
					<div class="clearfix"></div>
				</section>
				
				<section class="top-ctg-area clrlist">
					<div class="top-ctg-title">
						<h2>Top Categories</h2>
					</div>
					<div class="ctg__names clrlist">
						<ul>
							<li class="col-sm-4"><a href="#" class="ctg__ent">Entertainment</a></li>
							<li class="col-sm-4"><a href="#" class="ctg__family">Family</a></li>
							<li class="col-sm-4"><a href="#" class="ctg__business bdrr">Business</a></li>
							<li class="col-sm-3"><a href="#" class="ctg__sales bdrb">Sales & Promotion</a></li>
							<li class="col-sm-3"><a href="#" class="ctg__fit bdrb">Fitness</a></li>
							<li class="col-sm-3"><a href="#" class="ctg__chr bdrb">Charitable</a></li>
							<li class="col-sm-3"><a href="#" class="ctg__spirt bdrr bdrb">Spiritual</a></li>
						</ul>
					</div>
					<div class="clearfix"></div>
				</section>
				
				<section class="upcoming-area clrlist">
					<div class="upc__header">
						<div class="upc__title col-sm-6">
							<h2>Upcoming Events</h2>
						</div>
						<div class="upc__wwm col-sm-6">
							<ul class="pul-rgt">
								<li><a href="#">Week</a></li>
								<li><a href="#">Weekend</a></li>
								<li><a href="#">Monthly</a></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
					<div id="carousel-example-generic6" class="carousel" data-ride="carousel">
		  

		  <!-- Wrapper for slides -->
						  <div class="carousel-inner">


							<div class="item active client-bg">
								<div class="row">
										
									<div class="feat-box col-sm-4">
										<div class="feat__inr rotate-img--hover">
										
											<div class="feat__img">
												<img src="images/comingEvent1.jpg" alt="upcoming-event">
											</div>
											
											<div class="feat__share">
												<div class="feat__date col-sm-3">
													<strong>26</strong> Oct
												</div>
												<div class="feat__icons clrlist col-sm-9">
													<ul>
														<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
														<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
														<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
														<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
											
											<div class="feat__cont">
												<h3>Burlesque Wednesdays</h3>
												<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
												<div class="lnk-btn more-btn">
													<a href="#">Read More</a>
												</div>
											</div>
										</div>
									</div>
									
									<div class="feat-box col-sm-4">
										<div class="feat__inr rotate-img--hover">
										
											<div class="feat__img">
												<img src="images/comingEvent2.jpg" alt="upcoming-event">
											</div>
											
											<div class="feat__share">
												<div class="feat__date col-sm-3">
													<strong>26</strong> Oct
												</div>
												<div class="feat__icons clrlist col-sm-9">
													<ul>
														<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
														<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
														<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
														<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
											
											<div class="feat__cont">
												<h3>Burlesque Wednesdays</h3>
												<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
												<div class="lnk-btn more-btn">
													<a href="#">Read More</a>
												</div>
											</div>
										</div>
									</div>
									
									<div class="feat-box col-sm-4">
										<div class="feat__inr rotate-img--hover">
										
											<div class="feat__img">
												<img src="images/comingEvent3.jpg" alt="upcoming-event">
											</div>
											
											<div class="feat__share">
												<div class="feat__date col-sm-3">
													<strong>26</strong> Oct
												</div>
												<div class="feat__icons clrlist col-sm-9">
													<ul>
														<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
														<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
														<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
														<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
											
											<div class="feat__cont">
												<h3>Burlesque Wednesdays</h3>
												<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
												<div class="lnk-btn more-btn">
													<a href="#">Read More</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="item  client-bg">
								<div class="row">
										
									<div class="feat-box col-sm-4">
										<div class="feat__inr rotate-img--hover">
										
											<div class="feat__img">
												<img src="images/comingEvent1.jpg" alt="upcoming-event">
											</div>
											
											<div class="feat__share">
												<div class="feat__date col-sm-3">
													<strong>26</strong> Oct
												</div>
												<div class="feat__icons clrlist col-sm-9">
													<ul>
														<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
														<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
														<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
														<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
											
											<div class="feat__cont">
												<h3>Burlesque Wednesdays</h3>
												<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
												<div class="lnk-btn more-btn">
													<a href="#">Read More</a>
												</div>
											</div>
										</div>
									</div>
									
									<div class="feat-box col-sm-4">
										<div class="feat__inr rotate-img--hover">
										
											<div class="feat__img">
												<img src="images/comingEvent2.jpg" alt="upcoming-event">
											</div>
											
											<div class="feat__share">
												<div class="feat__date col-sm-3">
													<strong>26</strong> Oct
												</div>
												<div class="feat__icons clrlist col-sm-9">
													<ul>
														<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
														<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
														<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
														<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
											
											<div class="feat__cont">
												<h3>Burlesque Wednesdays</h3>
												<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
												<div class="lnk-btn more-btn">
													<a href="#">Read More</a>
												</div>
											</div>
										</div>
									</div>
									
									<div class="feat-box col-sm-4">
										<div class="feat__inr rotate-img--hover">
										
											<div class="feat__img">
												<img src="images/comingEvent3.jpg" alt="upcoming-event">
											</div>
											
											<div class="feat__share">
												<div class="feat__date col-sm-3">
													<strong>26</strong> Oct
												</div>
												<div class="feat__icons clrlist col-sm-9">
													<ul>
														<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
														<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
														<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
														<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
													</ul>
												</div>
												<div class="clearfix"></div>
											</div>
											
											<div class="feat__cont">
												<h3>Burlesque Wednesdays</h3>
												<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
												<div class="lnk-btn more-btn">
													<a href="#">Read More</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>	

							<div class="item  client-bg">
								<div class="row">
										
											<div class="feat-box col-sm-4">
												<div class="feat__inr rotate-img--hover">
												
													<div class="feat__img">
														<img src="images/comingEvent1.jpg" alt="upcoming-event">
													</div>
													
													<div class="feat__share">
														<div class="feat__date col-sm-3">
															<strong>26</strong> Oct
														</div>
														<div class="feat__icons clrlist col-sm-9">
															<ul>
																<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
																<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
																<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
																<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
															</ul>
														</div>
														<div class="clearfix"></div>
													</div>
													
													<div class="feat__cont">
														<h3>Burlesque Wednesdays</h3>
														<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
														<div class="lnk-btn more-btn">
															<a href="#">Read More</a>
														</div>
													</div>
												</div>
											</div>
											
											<div class="feat-box col-sm-4">
												<div class="feat__inr rotate-img--hover">
												
													<div class="feat__img">
														<img src="images/comingEvent2.jpg" alt="upcoming-event">
													</div>
													
													<div class="feat__share">
														<div class="feat__date col-sm-3">
															<strong>26</strong> Oct
														</div>
														<div class="feat__icons clrlist col-sm-9">
															<ul>
																<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
																<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
																<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
																<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
															</ul>
														</div>
														<div class="clearfix"></div>
													</div>
													
													<div class="feat__cont">
														<h3>Burlesque Wednesdays</h3>
														<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
														<div class="lnk-btn more-btn">
															<a href="#">Read More</a>
														</div>
													</div>
												</div>
											</div>
											
											<div class="feat-box col-sm-4">
												<div class="feat__inr rotate-img--hover">
												
													<div class="feat__img">
														<img src="images/comingEvent3.jpg" alt="upcoming-event">
													</div>
													
													<div class="feat__share">
														<div class="feat__date col-sm-3">
															<strong>26</strong> Oct
														</div>
														<div class="feat__icons clrlist col-sm-9">
															<ul>
																<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
																<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
																<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
																<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
															</ul>
														</div>
														<div class="clearfix"></div>
													</div>
													
													<div class="feat__cont">
														<h3>Burlesque Wednesdays</h3>
														<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
														<div class="lnk-btn more-btn">
															<a href="#">Read More</a>
														</div>
													</div>
												</div>
											</div>
										</div>
							</div>

							<div class="item  client-bg">
								<div class="row">
										
											<div class="feat-box col-sm-4">
												<div class="feat__inr rotate-img--hover">
												
													<div class="feat__img">
														<img src="images/comingEvent1.jpg" alt="upcoming-event">
													</div>
													
													<div class="feat__share">
														<div class="feat__date col-sm-3">
															<strong>26</strong> Oct
														</div>
														<div class="feat__icons clrlist col-sm-9">
															<ul>
																<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
																<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
																<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
																<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
															</ul>
														</div>
														<div class="clearfix"></div>
													</div>
													
													<div class="feat__cont">
														<h3>Burlesque Wednesdays</h3>
														<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
														<div class="lnk-btn more-btn">
															<a href="#">Read More</a>
														</div>
													</div>
												</div>
											</div>
											
											<div class="feat-box col-sm-4">
												<div class="feat__inr rotate-img--hover">
												
													<div class="feat__img">
														<img src="images/comingEvent2.jpg" alt="upcoming-event">
													</div>
													
													<div class="feat__share">
														<div class="feat__date col-sm-3">
															<strong>26</strong> Oct
														</div>
														<div class="feat__icons clrlist col-sm-9">
															<ul>
																<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
																<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
																<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
																<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
															</ul>
														</div>
														<div class="clearfix"></div>
													</div>
													
													<div class="feat__cont">
														<h3>Burlesque Wednesdays</h3>
														<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
														<div class="lnk-btn more-btn">
															<a href="#">Read More</a>
														</div>
													</div>
												</div>
											</div>
											
											<div class="feat-box col-sm-4">
												<div class="feat__inr rotate-img--hover">
												
													<div class="feat__img">
														<img src="images/comingEvent3.jpg" alt="upcoming-event">
													</div>
													
													<div class="feat__share">
														<div class="feat__date col-sm-3">
															<strong>26</strong> Oct
														</div>
														<div class="feat__icons clrlist col-sm-9">
															<ul>
																<li><a href="#" class="feat__fb"><i class="fa fa-facebook"></i></a></li>
																<li><a href="#" class="feat__pin"><i class="fa fa-pinterest-p"></i></a></li>
																<li><a href="#" class="feat__twt"><i class="fa fa-twitter"></i></a></li>
																<li><a href="#" class="feat__google"><i class="fa fa-google-plus"></i></a></li>
															</ul>
														</div>
														<div class="clearfix"></div>
													</div>
													
													<div class="feat__cont">
														<h3>Burlesque Wednesdays</h3>
														<p>Join the dapper and daring for Ottawa’s premiere weekly !</p>
														<div class="lnk-btn more-btn">
															<a href="#">Read More</a>
														</div>
													</div>
												</div>
											</div>
										</div>
							</div>			
							
						  </div>
<!--no slider
						  <a class="left carousel-control" href="#carousel-example-generic6" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left fa fa-angle-left"></span>
						  </a>
						  <a class="right carousel-control" href="#carousel-example-generic6" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right fa fa-angle-right"></span>
						  </a> -->
		</div>
						<div class="clearfix"></div>
				</section>
				
			</div>
			
			<div class="right-side col-sm-4">
				<div class="celeb-area">
					<img src="images/celebrating.jpg" alt="celebrating">
				</div>
				
				<div class="more-adv-area">
					<div class="adv__title">
						<h3>More Advertisings</h3>
					</div>
					<div class="adv__cont__main">
						<div class="adv__cont col-sm-6">
							<div class="adv__inr drp-shad">
								<div class="adv__img">
									<img src="images/moreAd1.jpg" alt="more-advertising">
								</div>
							</div>
						</div>
						<div class="adv__cont col-sm-6">
							<div class="adv__inr drp-shad">
								<div class="adv__img">
									<img src="images/moreAd2.jpg" alt="more-advertising">
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="adv__cont col-sm-6">
							<div class="adv__inr drp-shad">
								<div class="adv__img">
									<img src="images/moreAd3.jpg" alt="more-advertising">
								</div>
							</div>
						</div>
						<div class="adv__cont col-sm-6">
							<div class="adv__inr drp-shad">
								<div class="adv__img">
									<img src="images/moreAd4.jpg" alt="more-advertising">
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				<div class="news-ltr-area">
					<div class="news__title">
						<h4>Newsletter Subscribe</h4>
					</div>
					<div class="news__ctg">
						<div class="input-group">
							<select class="form-control" id="category">
								<option>Select Category</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>
						</div>
					</div>
					<div class="news__email">
						<div class="input-group">
							<input type="text" class="form-control" aria-label="..." placeholder="Your Email Address">
						</div>
					</div>
					<div class="news__email">
						<div class="input-group">
							<input type="submit" class="form-control" aria-label="..." value="Subscribe">
						</div>
					</div>
				</div>
				
				<div class="tweet-area-sidebar">
					<div class="tweet__inr">
						<html><head><title>IFRAME</title></head><body>
						<p id="p1">
						<a href="#pHRF">http://newsrss.bbc.co.uk/rss/newsonline_world_edition/front_page/rss.xml#</a>
						</p>
						<img src="images/rss.jpg" alt="rss">
						<iframe id="f1" src="view-source:http://newsrss.bbc.co.uk/rss/newsonline_world_edition/front_page/rss.xml#" height="95%" width="100%">
							
						</iframe>
						</body></html>

					</div>
				</div>
				
			</div>
			
		</div>
	</section>
	
	
	
	
	
<?php include("footer.php"); ?>